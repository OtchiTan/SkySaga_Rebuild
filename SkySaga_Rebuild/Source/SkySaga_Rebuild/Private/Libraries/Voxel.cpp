// Fill out your copyright notice in the Description page of Project Settings.


#include "Libraries/Voxel.h"

FVector UVoxel::ClampBlock(FVector BlockLocation)
{
	BlockLocation = FVector(
		FMath::TruncToFloat(BlockLocation.X / 100) * 100,
		FMath::TruncToFloat(BlockLocation.Y / 100) * 100,
		FMath::TruncToFloat(BlockLocation.Z / 100) * 100
	);
	if (BlockLocation.X < 0)
		BlockLocation.X -= 100;
	if (BlockLocation.Y < 0)
		BlockLocation.Y -= 100;
	return  BlockLocation;
}

int32 UVoxel::GetBlockIndex(int32 x, int32 y, int32 z)
{
	return ((x * 16 + y) * 256) + z;
}

FVector UVoxel::IndexToVector(int32 i)
{
	return FVector(i / (256 * 16), (i / 256) % 16, i % 256);
}

FVector2D UVoxel::GetChunkIndices(FVector WorldLocation)
{
	FVector2D ChunkIndex = FVector2D(
		FMath::TruncToFloat(WorldLocation.X / 1600),
		FMath::TruncToFloat(WorldLocation.Y / 1600)
		);
	if(WorldLocation.X < 0)
		ChunkIndex.X--;
	if(WorldLocation.Y < 0)
		ChunkIndex.Y--;
	return ChunkIndex;
}
