// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockGameInstance.h"

#include "AssetRegistry/AssetRegistryModule.h"
#include "MapSystem/BlockAsset.h"
#include "Inventory/ItemAsset.h"

UBlockGameInstance::UBlockGameInstance()
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	if (AssetRegistry.IsLoadingAssets())
	{
		AssetRegistry.OnFilesLoaded().AddUObject(this, &UBlockGameInstance::OnRegistryLoaded);
	}
	else
	{
		OnRegistryLoaded();
	}
}

void UBlockGameInstance::OnRegistryLoaded()
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	FARFilter Filter;
	Filter.ClassNames = {TEXT("BlockAsset"), TEXT("ItemAsset")};
	Filter.bRecursiveClasses = true;

	TArray<FAssetData> AssetList;
	AssetRegistry.GetAssets(Filter,AssetList);

	for (const FAssetData& Asset : AssetList)
	{
		UBlockAsset* Block = Cast<UBlockAsset>(Asset.GetAsset());
		if (Block)
		{
			BlockPossible.Add(Block->BlockID, Block);
		}
		else
		{
			UItemAsset* Item = Cast<UItemAsset>(Asset.GetAsset());
			if (Item)
			{
				ItemPossible.Add(Item->ID,Item);
			}
		}
	}

}
