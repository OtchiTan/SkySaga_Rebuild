// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/Crafter.h"

#include "Inventory/InventoryLibrary.h"

// Sets default values for this component's properties
UCrafter::UCrafter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCrafter::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCrafter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UItem* UCrafter::CraftItem(URecipeAsset* Recipe)
{
	UItem* ItemCrafted = nullptr;
	bool CanCreate = true;

	for (const TPair<UItemAsset*, int32> &Pair : Recipe->ItemsNeeded)
	{
		if (Inventory->GetQuantityItem(Pair.Key) < Pair.Value)
			CanCreate = false;
	}
	
	if (CanCreate)
	{
		ItemCrafted = UItem::CreateItem(Recipe->ItemToCraft,Recipe->QuantityCrafted);
		for (const TPair<UItemAsset*, int32> &Pair : Recipe->ItemsNeeded)
		{
			UItem* ItemTaked;
			Inventory->TakeItem(Pair.Key, Pair.Value, ItemTaked);
		}
	}
	return ItemCrafted;
}

