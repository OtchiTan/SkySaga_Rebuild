// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/InventoryLibrary.h"

bool UInventoryLibrary::CreateItem(UItemAsset* ItemAsset, const int32 Stack, UItem* &Item)
{
	Item = UItem::CreateItem(ItemAsset, Stack);
	return Item != nullptr;
}
