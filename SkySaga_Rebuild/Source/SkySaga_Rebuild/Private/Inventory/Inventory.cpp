// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/Inventory.h"

// Sets default values for this component's properties
UInventory::UInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventory::BeginPlay()
{
	Super::BeginPlay();

	Items.SetNum(SizeX * SizeY);
}


// Called every frame
void UInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventory::AddItemAt(UItem* ItemToAdd, int32 X, int32 Y, UItem*& RestItem)
{
	bool bResult;
	const int32 Index = X * SizeY + Y;
	const UItemAsset* ItemAsset= ItemToAdd->ItemAsset;
	if(Items.IsValidIndex(Index))
	{
		UItem* ItemInSlot = Items[Index];
		if (ItemInSlot == nullptr)
		{
			Items[Index] = ItemToAdd;
			bResult = true;
		}
		else if (ItemAsset->ID == ItemInSlot->ItemAsset->ID)
		{
			if (ItemInSlot->Stack + ItemToAdd->Stack <= ItemAsset->MaxStack)
			{
				ItemInSlot->Stack += ItemToAdd->Stack;
				bResult = true;
			}
			else
			{
				ItemInSlot->Stack = ItemAsset->MaxStack;
				RestItem = ItemToAdd;
				RestItem->Stack = ItemInSlot->Stack + ItemToAdd->Stack - ItemAsset->MaxStack;
				bResult = false;
			}
		}
		else
		{
			RestItem = ItemInSlot;
			Items[Index] = ItemToAdd;
			bResult = true;
		}
	}
	else
		bResult = false;
	OnInventoryUpdate.Broadcast();
	return bResult;
}

bool UInventory::RemoveItemAt(int32 X, int32 Y, UItem*& RemovedItem)
{
	const int32 Index = X * SizeY + Y;
	if (Items.IsValidIndex(Index))
	{
		RemovedItem = Items[Index];
		Items[Index] = nullptr;
		OnInventoryUpdate.Broadcast();
		return true;
	}
	else
	{
		return false;
	}
}

bool UInventory::AddItem(UItem* ItemToAdd, UItem*& RestItem)
{
	bool bSuccess;
	if (!IsValid(ItemToAdd))
		return false;
	int32 X,Y;
	while (GetAvailableSlot(ItemToAdd->ItemAsset,X,Y))
	{
		AddItemAt(ItemToAdd,X,Y,RestItem);
		if (RestItem != nullptr)
			ItemToAdd = RestItem;
		else
		{
			bSuccess = true;
			break;
		}
	}
	if (ItemToAdd != nullptr)
	{
		if (GetFreeSlot(X,Y))
		{
			AddItemAt(ItemToAdd,X,Y,RestItem);
			bSuccess = true;
		}
		else
			bSuccess = false;
	}
	else
		bSuccess = true;

	OnInventoryUpdate.Broadcast();
	return bSuccess;
}

bool UInventory::GetAvailableSlot(const UItemAsset* ItemAsset, int32& X, int32& Y)
{
	if (!IsValid(ItemAsset))
		return false;
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			UItem* ItemInSlot = Items[Index];
			if (ItemInSlot != nullptr && ItemAsset != nullptr && ItemInSlot->ItemAsset->ID == ItemAsset->ID && !ItemInSlot->IsFullStack())
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool UInventory::GetFreeSlot(int32& X, int32& Y)
{
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			if (Items[Index] == nullptr)
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool UInventory::TakeItemAt(int32 X, int32 Y, int32 Quantity, UItem*& QuantityTaked)
{
	const int32 Index = X * SizeY + Y;
	UItem* ItemInSlot = Items[Index];
	if (ItemInSlot != nullptr)
	{
		if (ItemInSlot->Stack - Quantity == 0)
		{
			QuantityTaked = ItemInSlot;
			Items[Index] = nullptr;
		}
		else if (ItemInSlot->Stack - Quantity > 0)
		{
			QuantityTaked = UItem::CreateItem(ItemInSlot->ItemAsset, Quantity);
			Items[Index]->Stack -= Quantity;
		}
		else
		{
			QuantityTaked = UItem::CreateItem(ItemInSlot->ItemAsset, ItemInSlot->Stack);
			Items[Index] = nullptr;
		}
		OnInventoryUpdate.Broadcast();
		return true;
	}
	else
		return false;
}

bool UInventory::TakeItem(UItemAsset* ItemAsset, int32 Quantity, UItem*& ItemTaked)
{
	int32 i = 0;
	ItemTaked = UItem::CreateItem(ItemAsset,0);
	for (UItem* Item : Items)
	{
		if (IsValid(Item) && Item->ItemAsset->ID == ItemAsset->ID)
		{
			if (Item->Stack > Quantity)
			{
				ItemTaked->Stack = Quantity;
				Item->Stack -= Quantity;
				break;
			}
			else if (Item->Stack == Quantity)
			{
				ItemTaked->Stack = Quantity;
				Items[i] = nullptr;
				break;
			}
			else
			{
				ItemTaked->Stack += Item->Stack;
				Items[i] = nullptr;
			}
		}
		i++;
	}
	
	OnInventoryUpdate.Broadcast();
	return  Quantity <= 0;
}

int32 UInventory::GetQuantityItem(const UItemAsset* ItemAsset)
{
	int32 Count = 0;

	for (const UItem* Item : Items)
	{
		if (IsValid(Item) && Item->ItemAsset->ID == ItemAsset->ID)
			Count += Item->Stack;
	}
	
	return Count;
}
