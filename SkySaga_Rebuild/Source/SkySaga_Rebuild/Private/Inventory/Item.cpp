// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/Item.h"

UItem* UItem::CreateItem(UItemAsset* ItemAsset, int32 Stack)
{
	UItem* i = NewObject<UItem>();
	i->ItemAsset = ItemAsset;
	i->Stack = Stack;
	return i;
}

bool UItem::IsFullStack()
{
	return Stack == ItemAsset->MaxStack;
}
