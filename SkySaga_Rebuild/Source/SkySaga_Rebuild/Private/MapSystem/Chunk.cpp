// Fill out your copyright notice in the Description page of Project Settings.


#include "MapSystem/Chunk.h"
#include "BlockGameInstance.h"
#include "Components/BoxComponent.h"
#include "Engine/AssetManager.h"
#include "Libraries/Voxel.h"
#include "Libraries/PerlinLibrary.h"
#include "MapSystem/WorldManager.h"

// Sets default values
AChunk::AChunk()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	ProceduralMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Procedural Mesh"));
	ProceduralMesh->SetupAttachment(RootComponent);

	UBoxComponent* BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetupAttachment(RootComponent);
	BoxComponent->AddLocalOffset(FVector(800,800,12800));
	BoxComponent->SetBoxExtent(FVector(800,800,12800));

	const auto Material = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Blueprint/M_Test.M_Test'"));
	ChunkMat = Cast<UMaterialInterface>(Material.Object);
	
	Vertices.Reserve(1572864);
	Normals.Reserve(1572864);
	UV0.Reserve(1572864);
	Tangents.Reserve(1572864);
	Triangles.Reserve(2359296);
}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	Super::BeginPlay();
}

void AChunk::CalculateChunk()
{
	const UBlockGameInstance* GameInstance = Cast<UBlockGameInstance>(GetWorld()->GetGameInstance());
	TMap<TEnumAsByte<EBlockType>, UBlockAsset*> BlockPossible = GameInstance->BlockPossible;
	
	BlockList.Reset();
	BlockList.SetNum(65536);
	
	AsyncTask(ENamedThreads::NormalTaskPriority, [this, BlockPossible] ()
	{
		for (int32 x = 0; x < 16; ++x)
		{
			for (int32 y = 0; y < 16; ++y)
			{
                const int32 GlobalX = x + ChunkLocation.X / 100;
                const int32 GlobalY = y + ChunkLocation.Y / 100;
                const int32 PerlinZ = UPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,Scale,Amplitude);
				const int32 Dist = FMath::Sqrt(FMath::Square(GlobalX * 100 / SizeMax) + FMath::Square(GlobalY * 100 / SizeMax));
				int32 MaxPerlinZ = PerlinZ;
				if (Dist >= 90)
					MaxPerlinZ -= Dist - 90 / 10;
				for (int32 z = 0; z < 256; ++z)
				{
					const int32 BlockIndex = UVoxel::GetBlockIndex(x,y,z);
					if (z >= 128 - MaxPerlinZ && z <= 128 + MaxPerlinZ)
					{
						if (z == 128 + MaxPerlinZ) BlockList[BlockIndex] = BlockPossible.FindRef(Grass); 
						else if (z > 128 + MaxPerlinZ - 3) BlockList[BlockIndex] = BlockPossible.FindRef(Dirt); 
						else BlockList[BlockIndex] = BlockPossible.FindRef(Stone); 
					}
					else
					{
						BlockList[BlockIndex] = BlockPossible.FindRef(Air); 
					}
				}
			}
		}
		ChunkGenerator->GenerateChunks.Enqueue(this);
	});
}

void AChunk::Load(FVector WorldLocation, FVector2D NewPos)
{
	ChunkLocation = WorldLocation;
	ProceduralMesh->SetWorldLocation(ChunkLocation);
	CalculateChunk();
}

void AChunk::ReLoad(FVector WorldLocation, TArray<UBlockAsset*> InBlockList, FVector2D NewPos)
{
	ChunkLocation = WorldLocation;
	ProceduralMesh->SetWorldLocation(ChunkLocation);
	BlockList = InBlockList;
	ChunkIndex = NewPos;
	RenderChunk();
}

void AChunk::Initialize(AWorldManager* InChunkGenerator,FVector WorldLocation, TArray<UBlockAsset*> InBlockList, FVector2D NewPos)
{
	ChunkGenerator = InChunkGenerator;
	ChunkLocation = WorldLocation;
	ChunkIndex = NewPos;
	ProceduralMesh->SetWorldLocation(ChunkLocation);
	ProceduralMesh->bUseAsyncCooking = true;
	if (InBlockList.Num() != 0)
	{
		BlockList = InBlockList;
	
		RenderChunk();
	}
	else
	{
		CalculateChunk();
	}
}

void AChunk::RenderChunk()
{
	Vertices.Reset();
	Normals.Reset();
	UV0.Reset();
	Tangents.Reset();
	Triangles.Reset();

	if (BlockList.Num() == 0) return;
	AsyncTask(ENamedThreads::NormalTaskPriority,[this]
	{
		int32 Offset = 0;
	
		for (int32 x = 0; x < 16; ++x)
		{
			for (int32 y = 0; y < 16; ++y)
			{
				for (int32 z = 0; z < 256; ++z)
				{
					const int32 BlockIndex = UVoxel::GetBlockIndex(x,y,z);
					if (BlockList[BlockIndex]->BlockID != Air)
					{
						GenerateBlock(x,y,z,Offset, BlockList[BlockIndex]);
					}
				}
			}
		}
		ChunkGenerator->CalculatedChunks.Enqueue(this);
	});
}

UBlockAsset* AChunk::ChangeBlockType(FVector LocalOffset, UBlockAsset* NewBlock)
{
	LocalOffset.X = LocalOffset.X > 0 ? FMath::TruncToFloat(LocalOffset.X / 100) : FMath::TruncToFloat(LocalOffset.X / 100) / -1;
	LocalOffset.Y = LocalOffset.Y > 0 ? FMath::TruncToFloat(LocalOffset.Y / 100) : FMath::TruncToFloat(LocalOffset.Y / 100) / -1;
	LocalOffset.Z = LocalOffset.Z > 0 ? FMath::TruncToFloat(LocalOffset.Z / 100) : FMath::TruncToFloat(LocalOffset.Z / 100) / -1;
	const int32 BlockIndex = UVoxel::GetBlockIndex(LocalOffset.X, LocalOffset.Y, LocalOffset.Z);

	UBlockAsset* OldBlock = BlockList[BlockIndex];
	BlockList[BlockIndex] = NewBlock;

	RenderChunk();
	return OldBlock;
}

void AChunk::GenerateBlock(int32 x, int32 y, int32 z, int32 &Offset,UBlockAsset* Block){
	const FVector BlockLocation = FVector(x,y,z) * 100;
	
	for (const FFaces &Face : Block->Faces)
	{
		if (Face.LocationToCull != FVector::ZeroVector)
		{
			const FVector CullIndex = Face.LocationToCull + FVector(x,y,z);
			if (IsTransparent(CullIndex.X,CullIndex.Y,CullIndex.Z))
			{
				for (const int32 Triangle : Face.Triangles)
				{
					Triangles.Add(Triangle + Offset);
				}
				for (const FVector &Vertex : Face.Vertices)
				{
					Vertices.Add(Vertex + BlockLocation);
					Normals.Add(FVector(
						Vertex.X == 0 ? NegativeCosNormals : CosNormals,
						Vertex.Y == 0 ? NegativeCosNormals : CosNormals,
						Vertex.Z == 0 ? NegativeCosNormals : CosNormals
					));
					Tangents.Add(FProcMeshTangent(
						Vertex.X == 0 ? CosNormals : NegativeCosNormals,
						Vertex.Y == 0 ? CosNormals : NegativeCosNormals,
						Vertex.Z == 0 ? CosNormals : NegativeCosNormals)
						);
					Offset++;
				}
				for (const FVector2D &FaceUV : Face.UV)
				{
					UV0.Add(FVector2D(
						FaceUV.X / 4,
						(FaceUV.Y + (Block->AtlasIndex * 3)) / (NumTexture * 3)
					));
				}
			}
		}
	}
}

bool AChunk::IsTransparent(const int32 X,const int32 Y,const int32 Z)
{
	if (X < 0 || X >= 16 || Y < 0 || Y >= 16 || Z < 0 || Z >= 256)
		return true;
	return BlockList[UVoxel::GetBlockIndex(X,Y,Z)]->BlockID == Air;
}

void AChunk::ShowChunk()
{
	ProceduralMesh->CreateMeshSection(0,Vertices, Triangles, Normals, UV0, VertexColors, Tangents, true);

	ProceduralMesh->SetMaterial(0,ChunkMat);
}

void AChunk::HideChunk()
{
	ProceduralMesh->ClearMeshSection(0);
}
