// Fill out your copyright notice in the Description page of Project Settings.


#include "MapSystem/WorldManager.h"

#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Libraries/Voxel.h"
#include "MapSystem/Chunk.h"

// Sets default values for this component's properties
AWorldManager::AWorldManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;

	// ...
}

void AWorldManager::TickActor(float DeltaTime, ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	AChunk* Chunk = nullptr; 
	while (GenerateChunks.Dequeue(Chunk))
	{
		Chunk->RenderChunk();
	}
	while (CalculatedChunks.Dequeue(Chunk))
	{
		Chunk->ShowChunk();
	}
}

// Called when the game starts
void AWorldManager::BeginPlay()
{
	Super::BeginPlay();

	SpawnBaseChunks();
	
}



void AWorldManager::SpawnBaseChunks()
{
	for (int x = 0 - RenderDistance / 2; x < RenderDistance / 2; ++x)
	{
		for (int y = 0 - RenderDistance / 2; y <  RenderDistance / 2; ++y)
		{
			SpawnChunk(x,y);
		}
	}
}

bool AWorldManager::ChangeBlockType(const FVector WorldLocation, UBlockAsset* NewBlock, UBlockAsset*& BlockChanged)
{
	if (!NewBlock)
		return false;
	
	const FVector2D ChunkPos = UVoxel::GetChunkIndices(WorldLocation);
	
	if (LoadedChunks.Contains(ChunkPos))
	{
		AChunk* Chunk = LoadedChunks.FindRef(ChunkPos);
		const FVector LocalOffset = WorldLocation - Chunk->ChunkLocation;
		BlockChanged = Chunk->ChangeBlockType(LocalOffset, NewBlock);
		return true;
	}
	else
		return false;
}

void AWorldManager::ReRenderChunks(FVector WorldLocation)
{
	const FVector2D BaseChunkIndex = UVoxel::GetChunkIndices(WorldLocation);
	if (BaseChunkIndex == ActualChunkPos)
		return;
	ActualChunkPos = BaseChunkIndex;
	TMap<FVector2D, AChunk*> ChunksToMove = TMap<FVector2D,AChunk*>(LoadedChunks);
	TArray<FVector2D> NeedChunksLocations = {};
	for (int x = BaseChunkIndex.X - RenderDistance / 2; x < BaseChunkIndex.X + RenderDistance / 2; ++x)
	{
		for (int y = BaseChunkIndex.Y - RenderDistance / 2; y < BaseChunkIndex.Y + RenderDistance / 2; ++y)
		{
			const FVector2D ChunkIndex = FVector2D(x,y);
			if (!LoadedChunks.Contains(ChunkIndex))
			{
				NeedChunksLocations.Add(ChunkIndex);
			}
			else
			{
				ChunksToMove.Remove(ChunkIndex);
			}
		}
	}
	int32 i = 0;
	for (const TPair<FVector2D,AChunk*> &ToMove : ChunksToMove)
	{
		const FVector2D NextChunk = NeedChunksLocations[i];
		MoveChunk(ToMove.Key.X,ToMove.Key.Y, NextChunk.X,NextChunk.Y);
		i++;
	}
}

void AWorldManager::SpawnChunk(int32 x, int32 y)
{
	AChunk* Chunk = SpawnedChunks.FindRef(FVector2D(x,y));
	if (!IsValid(Chunk))
	{
		Chunk = GWorld->SpawnActor<AChunk>(AChunk::StaticClass(), FVector(x * 1600, y * 1600, 0), FRotator(0));
		
		const FSavedChunk* ChunkSaved = ChunkList.Find(FVector2D(x,y));
		const TArray<UBlockAsset*> Blocks = ChunkSaved != nullptr ? ChunkSaved->Blocks : TArray<UBlockAsset*>();
		
		LoadedChunks.Add(FVector2D(x,y),Chunk);
		Chunk->Initialize(this, FVector(x * 1600, y * 1600,0),Blocks, FVector2D(x,y));
	}
	else
	{
		Chunk->RenderChunk();
		LoadedChunks.Add(FVector2D(x,y),Chunk);
	}
}

void AWorldManager::OnChunkGenerate(AChunk* Chunk)
{
	GenerateChunks.Dequeue(Chunk);

	Chunk->RenderChunk();
}

void AWorldManager::MoveChunk(const int32 OldX, const int32 OldY, const int32 NewX, const int32 NewY)
{
	const FVector2D OldPos = FVector2D(OldX,OldY);
	const FVector2D NewPos = FVector2D(NewX,NewY);
	const FVector NewWorldLocation = FVector(NewX * 1600, NewY * 1600, 0);

	// Sauvegarde du chunk
	AChunk* Chunk = LoadedChunks.FindRef(OldPos);
	FSavedChunk SavedChunk;
	SavedChunk.Blocks = Chunk->BlockList;
	SavedChunk.ChunkLocation = OldPos;
	ChunkList.Add(OldPos, SavedChunk);

	// Initialisation du nouveau chunk
	Chunk->HideChunk();
	Chunk->SetActorLocation(NewWorldLocation);
	LoadedChunks.Remove(OldPos);
	LoadedChunks.Add(NewPos, Chunk);
	if (ChunkList.Contains(NewPos))
	{
		const FSavedChunk ChunkSaved = ChunkList.FindRef(NewPos); 
		Chunk->ReLoad(NewWorldLocation, ChunkSaved.Blocks, NewPos);
	}
	else
	{
		Chunk->Load(NewWorldLocation, NewPos);
	}
}


