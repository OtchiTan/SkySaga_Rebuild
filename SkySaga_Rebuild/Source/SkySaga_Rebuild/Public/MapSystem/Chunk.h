// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlockAsset.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Chunk.generated.h"

USTRUCT(BlueprintType)
struct SKYSAGA_REBUILD_API FSavedChunk
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UBlockAsset*> Blocks;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector2D ChunkLocation;
};

class AWorldManager;
UCLASS()
class SKYSAGA_REBUILD_API AChunk : public AActor
{
	GENERATED_BODY()

private:
	const float CosNormals = FMath::Cos(45); 
	const float NegativeCosNormals = FMath::Cos(-45);
	
	TArray<FVector> Vertices = {};
	TArray<int32> Triangles = {};
	TArray<FVector> Normals = {};
	TArray<FVector2D> UV0 = {};
	const TArray<FColor> VertexColors = {};
	TArray<FProcMeshTangent> Tangents = {};
	
public:	
	// Sets default values for this actor's properties
	AChunk();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	
	void CalculateChunk();

	void Load(FVector WorldLocation, FVector2D NewPos);

	void ReLoad(FVector WorldLocation, TArray<UBlockAsset*> InBlockList, FVector2D NewPos);

	void Initialize(AWorldManager* InChunkGenerator,FVector WorldLocation, TArray<UBlockAsset*> InBlockList, FVector2D NewPos);

	UFUNCTION(BlueprintCallable)
	void RenderChunk();

	UFUNCTION(BlueprintCallable)
	UBlockAsset* ChangeBlockType(FVector LocalOffset, UBlockAsset* NewBlock);

	void GenerateBlock(int32 x, int32 y, int32 z, int32 &Offset,UBlockAsset* Block);

	inline bool IsTransparent(int32 X, int32 Y, int32 Z);

	void ShowChunk();
	void HideChunk();

	int32 Scale = 50;
	int32 Amplitude = 20;
	int32 Seed = 0;
	int32 SizeMax = 100;
	int32 NumTexture = 10;
	FVector2D ChunkIndex;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<UBlockAsset*> BlockList;

	UPROPERTY()
	UMaterialInterface* ChunkMat;
	UPROPERTY()
	UProceduralMeshComponent* ProceduralMesh;

	UPROPERTY(BlueprintReadOnly)
	FVector ChunkLocation;

	UPROPERTY(BlueprintReadOnly)
	AWorldManager* ChunkGenerator;
};
