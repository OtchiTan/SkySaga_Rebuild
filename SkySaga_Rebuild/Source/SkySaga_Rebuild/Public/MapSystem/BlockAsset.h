// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Engine/DataAsset.h"
#include "BlockAsset.generated.h"

class UItemAsset;

USTRUCT(BlueprintType)
struct SKYSAGA_REBUILD_API FFaces
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FVector> Vertices;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int32> Triangles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector LocationToCull {0,0,0};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FVector2D> UV;
};

UENUM(BlueprintType)
enum EBlockType
{
	Air UMETA(DisplayName= "Air"),
	Dirt UMETA(DisplayName= "Dirt"),
	Grass UMETA(DisplayName= "Grass"),
	Stone UMETA(DisplayName= "Stone"),
	Wood UMETA(DisplayName= "Wood"),
	Leaf UMETA(DisplayName= "Leaf"),
	Plank UMETA(DisplayName= "Plank"),
};

/**
 * 
 */
UCLASS(BlueprintType)
class SKYSAGA_REBUILD_API UBlockAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TEnumAsByte<EBlockType> BlockID;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FFaces> Faces;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UItemAsset* ItemAsset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 AtlasIndex;
};
