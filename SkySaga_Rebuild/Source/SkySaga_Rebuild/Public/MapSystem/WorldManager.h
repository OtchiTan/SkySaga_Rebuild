// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Chunk.h"
#include "Components/ActorComponent.h"
#include "WorldManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKYSAGA_REBUILD_API AWorldManager : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	AWorldManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickActor(float DeltaTime, ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	UPROPERTY(BlueprintReadOnly)
	TMap<FVector2D,AChunk*> LoadedChunks;
	
	UPROPERTY(BlueprintReadOnly)
	TMap<FVector2D,AChunk*> SpawnedChunks;
	
	UPROPERTY(BlueprintReadOnly)
	TMap<FVector2D,FSavedChunk> ChunkList;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 RenderDistance = 16;

	FVector2D ActualChunkPos = FVector2D::ZeroVector;

	TQueue<AChunk*> GenerateChunks;
	TQueue<AChunk*> CalculatedChunks;
	
	void SpawnBaseChunks();

	UFUNCTION(BlueprintCallable)
	bool ChangeBlockType(FVector WorldLocation, UBlockAsset* NewBlock, UBlockAsset*& BlockChanged);

	UFUNCTION(BlueprintCallable)
	void ReRenderChunks(FVector WorldLocation);

	UFUNCTION(BlueprintCallable)
	void SpawnChunk(int32 x, int32 y);

	void OnChunkGenerate(AChunk* Chunk);
	
	void MoveChunk(int32 OldX, int32 OldY, int32 NewX, int32 NewY);
};
