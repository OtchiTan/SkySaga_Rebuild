// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Inventory/ItemAsset.h"
#include "InventoryLibrary.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYSAGA_REBUILD_API UInventoryLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static bool CreateItem(UItemAsset* ItemAsset, const int32 Stack, UItem* &Item);
};
