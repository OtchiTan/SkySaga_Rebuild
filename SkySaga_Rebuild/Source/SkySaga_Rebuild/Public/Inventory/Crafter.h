// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Inventory.h"
#include "RecipeAsset.h"
#include "Components/ActorComponent.h"
#include "Crafter.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKYSAGA_REBUILD_API UCrafter : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCrafter();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite)
	UInventory* Inventory;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<URecipeAsset*> Recipes;

	UFUNCTION(BlueprintCallable)
	UItem* CraftItem(URecipeAsset* Recipe);
};
