// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Inventory/ItemAsset.h"
#include "Item.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYSAGA_REBUILD_API UItem : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static UItem* CreateItem(UItemAsset* ItemAsset, int32 Stack);
	
	UPROPERTY(BlueprintReadWrite)
	UItemAsset* ItemAsset;

	UPROPERTY(BlueprintReadWrite)
	int32 Stack = 1;

	UFUNCTION(BlueprintCallable)
	bool IsFullStack();
};
