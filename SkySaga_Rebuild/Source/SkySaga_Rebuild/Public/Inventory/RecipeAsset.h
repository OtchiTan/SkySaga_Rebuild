// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemAsset.h"
#include "Engine/DataAsset.h"
#include "RecipeAsset.generated.h"

UENUM(BlueprintType)
enum FRecipe
{
	Planks UMETA(DisplayName= "Planks"),
	Stick UMETA(DisplayName= "Stick"),
	Pickaxe UMETA(DisplayName= "Pickaxe"),
};

/**
 * 
 */
UCLASS(BlueprintType)
class SKYSAGA_REBUILD_API URecipeAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TEnumAsByte<FRecipe> ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName RecipeName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<UItemAsset*, int32> ItemsNeeded;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UItemAsset* ItemToCraft;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 QuantityCrafted;
};
