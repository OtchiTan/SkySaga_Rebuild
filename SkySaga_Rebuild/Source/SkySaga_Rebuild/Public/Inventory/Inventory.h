// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Components/ActorComponent.h"
#include "Inventory/ItemAsset.h"
#include "Inventory.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInventoryUpdate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class SKYSAGA_REBUILD_API UInventory : public UActorComponent
{
	
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventory();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite)
	TArray<UItem*> Items;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 SizeX = 5;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 SizeY = 10;

	UFUNCTION(BlueprintCallable)
	virtual bool AddItemAt(UItem* ItemToAdd, int32 X, int32 Y, UItem* &RestItem);

	UFUNCTION(BlueprintCallable)
	bool RemoveItemAt(int32 X, int32 Y, UItem* &RemovedItem);

	UFUNCTION(BlueprintCallable)
	virtual bool AddItem(UItem* ItemToAdd, UItem*& RestItem);

	bool GetAvailableSlot(const UItemAsset* ItemAsset, int32& X, int32& Y);

	bool GetFreeSlot(int32& X, int32& Y);

	UFUNCTION(BlueprintCallable)
	virtual bool TakeItemAt(int32 X, int32 Y, int32 Quantity, UItem*& QuantityTaked);

	UFUNCTION(BlueprintCallable)
	virtual bool TakeItem(UItemAsset* ItemAsset, int32 Quantity, UItem*& ItemTaked);

	UFUNCTION(BlueprintCallable)
	int32 GetQuantityItem(const UItemAsset* ItemAsset);

	UPROPERTY(BlueprintAssignable)
	FOnInventoryUpdate OnInventoryUpdate;
};
