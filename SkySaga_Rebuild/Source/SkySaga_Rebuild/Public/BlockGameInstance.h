// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MapSystem/BlockAsset.h"
#include "Engine/GameInstance.h"
#include "BlockGameInstance.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYSAGA_REBUILD_API UBlockGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UBlockGameInstance();

	UPROPERTY(BlueprintReadOnly)
	TMap<TEnumAsByte<EBlockType>,UBlockAsset*> BlockPossible;

	UPROPERTY(BlueprintReadOnly)
	TMap<FName,UItemAsset*> ItemPossible;

	void OnRegistryLoaded();
};
