// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Voxel.generated.h"

/**
 * 
 */
UCLASS()
class SKYSAGA_REBUILD_API UVoxel : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FVector ClampBlock(FVector BlockLocation);

	UFUNCTION(BlueprintCallable)
	static int32 GetBlockIndex(int32 x, int32 y, int32 z);

	UFUNCTION(BlueprintCallable)
	static FVector IndexToVector(int32 i);

	UFUNCTION(BlueprintCallable)
	static FVector2D GetChunkIndices(FVector WorldLocation);
};
