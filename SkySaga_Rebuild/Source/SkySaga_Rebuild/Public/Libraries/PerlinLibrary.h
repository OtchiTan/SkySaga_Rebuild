// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PerlinLibrary.generated.h"

/**
 * 
 */
UCLASS()
class SKYSAGA_REBUILD_API UPerlinLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

	UFUNCTION(BlueprintPure, meta = (DisplayName = "1D Perlin Noise", Keywords = "One 1D Perlin_Noise perlin noise Perlin Noise", 
		ToolTip = "Sample 1D perlin noise at the given X coordinate."), Category = "Perlin_Noise")
	static float OneD_Perlin_Noise(const float x, const float scale = 1, const float amplitude = 1);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "2D Perlin Noise", Keywords = "Two 2D Perlin_Noise perlin noise Perlin Noise",
		ToolTip = "Sample 2D perlin noise at the given X,Y coordinate."), Category = "Perlin_Noise")
	static float TwoD_Perlin_Noise(const float x, const float y, const float scale = 1, const float amplitude = 1);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "3D Perlin Noise", Keywords = "Three 3D Perlin_Noise perlin noise Perlin Noise", 
		ToolTip = "Sample 3D perlin noise at the given X,Y,Z coordinate."), Category = "Perlin_Noise")
	static float ThreeD_Perlin_Noise(const float x, const float y, const float z, const float scale = 1, const float amplitude = 1);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "4D Perlin Noise", Keywords = "Four 4D Perlin_Noise perlin noise Perlin Noise", 
		ToolTip = "Sample 4D perlin noise at the given X,Y,Z,W coordinate."), Category = "Perlin_Noise")
	static float FourD_Perlin_Noise(const float x, const float y, const float z, const float w, const float scale = 1, const float amplitude = 1);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "1D Perlin Fractal", Keywords = "One 1D Perlin_Noise perlin noise fractal",
		ToolTip = "Sample 1D fractal perlin noise at the given X coordinate.", AdvancedDisplay = 4), Category = "Perlin_Noise")
	static float OneD_Perlin_Fractal(const float x, const int levels, float scale = 1, float amplitude = 1, const float ScaleFade = 2, const float AmpFade = 2);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "2D Perlin Fractal", Keywords = "Two 2D Perlin_Noise perlin noise fractal", 
		ToolTip = "Sample 2D fractal perlin noise at the given X,Y coordinate.", AdvancedDisplay = 5), Category = "Perlin_Noise")
	static float TwoD_Perlin_Fractal(const float x, const float y, const int levels, float scale = 1, float amplitude = 1, const float ScaleFade = 2, const float AmpFade = 2);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "3D Perlin Fractal", Keywords = "Three 3D Perlin_Noise perlin noise fractal", 
		ToolTip = "Sample 3D fractal perlin noise at the given X,Y,Z coordinate.", AdvancedDisplay = 6), Category = "Perlin_Noise")
	static float ThreeD_Perlin_Fractal(const float x, const float y, const float z, const int levels, float scale = 1, float amplitude = 1, const float ScaleFade = 2, const float AmpFade = 2);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "4D Perlin Fractal", Keywords = "Four 4D Perlin_Noise perlin noise fractal", 
		ToolTip = "Sample 4D fractal perlin noise at the given X,Y,Z,W coordinate.", AdvancedDisplay = 7), Category = "Perlin_Noise")
	static float FourD_Perlin_Fractal(const float x, const float y, const float z, const float w, const int levels, float scale = 1, float amplitude = 1, const float ScaleFade = 2, const float AmpFade = 2);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Noise Seed", Keywords = "Set_Seed seed set seed", ToolTip = "Set the seed for noise generation."), Category = "Perlin_Noise")
	static void SetSeed(int seed);
};
